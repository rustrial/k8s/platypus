#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

set -x
set -euo pipefail

case "$(uname -m)" in
    x86_64|amd64)
        linux_arch="x86_64"
        go_arch="amd64"
    ;;
    aarch64|arm64)
        linux_arch="aarch64"    
        go_arch="arm64"
    ;;
esac

mkdir ~/bin

# download kubebuilder and install locally.
curl -L -o ~/bin/kubebuilder "https://github.com/kubernetes-sigs/kubebuilder/releases/download/v3.7.0/kubebuilder_$(go env GOOS)_$(go env GOARCH)"
chmod +x ~/bin/kubebuilder
