#!/bin/bash

TMP_REPOSITORY="61529cb9-f049-4788-81d2-c4409d7c893f"

hashsum(){
    command -v sha3sum || command -v sha1sum || command -v md5sum || command -v shasum || command -v md5
}

# Create reproducible (stable) hash sum over the content of a directory tree.
hashdir(){
    find $1 -type f -exec $(hashsum) {} \; | sort -k 2 | $(hashsum) | awk '{print $1}'
}

toolchain_id(){
    hashdir toolchain
}

toolchain(){
    local tid=$1
    [[ -z "$tid" ]] && tid=$(toolchain_id)
    local toolchain=$(docker image ls -f reference=$tid -q)
    [[ -n "$toolchain" ]] || (cd toolchain && docker image build -t $tid .)
}

deploy_toolchain(){
    local tid="$(toolchain_id)"
    local stable_name=$1
    local remote="${CI_REGISTRY_IMAGE}:toolchain-$(uname -m)-${tid}"
    if ! docker manifest inspect $remote >/dev/null; then
        toolchain $tid && \
        docker image tag $tid $stable_name && \
        docker image tag $tid $remote && \
        docker image push $stable_name && \
        docker image push $remote
    else # The toolchain already exists and just needs to be aliased (tagged)
         # with the current commits SHA
        docker image pull $remote && \
        docker image tag $remote $stable_name && \
        docker image push $stable_name
    fi
}


main(){
    if [[ -z "$CI" ]]; then
        # If running outside CI/CD fallback to current branch name
        CI_COMMIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
        if [[ -e ~/.gitconfig ]]; then
          GITCONFIG_VOLUME="-v $(cd && pwd)/.gitconfig:/root/.gitconfig"
        fi
        local git_slug=$(echo $CI_COMMIT_BRANCH | tr -d '[[:space:]]' | tr -c '[[:alnum:]]' '-')
        export CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME:-${CI_COMMIT_BRANCH:-${CI_COMMIT_TAG}}}
        export CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG:-${git_slug}}
        export CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --verify HEAD | head -c 12)}
        local tid="$(toolchain_id)"
        toolchain $tid && exec docker container run --rm -it --privileged $GITCONFIG_VOLUME \
          --workdir /workdir \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v $(pwd):/workdir \
          --mount type=tmpfs,destination=/tmp \
          --entrypoint /workdir/ci.sh -e CI=true \
          -e CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} \
          -e CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} \
          -e CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH} \
          -e FEATURE_BRANCH=${FEATURE_BRANCH} \
          -e CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} \
          -e DEBUG=${DEBUG} \
          $tid "$@"
    else        
        "$@"
    fi
}

[[ -n "$DEBUG" ]] && set -x
main "$@"
